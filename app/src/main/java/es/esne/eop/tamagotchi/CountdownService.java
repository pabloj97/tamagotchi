package es.esne.eop.tamagotchi;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class CountdownService extends Service {

    private static final String TAG=CountDownTimer.class.getCanonicalName();
    static final String FIN_CONTADOR="fin";
    private static final int idUnica = 516256453;
    private static final String CHANNEL_ID = "notificaciones";
    private static final CharSequence name = "noti";
    NotificationCompat.Builder notification;

    public CountdownService()
    {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(TAG,"onStartCommand");
        new Contador(5000,1000).start();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    class Contador extends CountDownTimer
    {
        Contador(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
            Log.d(TAG,"millisInFuture: "+millisInFuture+" ,, countDownInterval: "+countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            Log.d(TAG,"onTick. millisUntilFinished: "+millisUntilFinished);
        }

        @Override
        public void onFinish()
        {
            Log.d(TAG,"onFinish");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                crearNotificacion();
            else{
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra(FIN_CONTADOR,true);
                intent.putExtra("TamagotchiHappiness", false);
                startActivity(intent);
            }
        }
    }

    /**
     * Función que maneja la gestión de notificaciones en el movil.
     */
    public void crearNotificacion()
    {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);

        intent.putExtra("b", false);

        notification = new NotificationCompat.Builder(CountdownService.this, CHANNEL_ID);
        notification.setAutoCancel(true);
        notification.setContentTitle("Tamagotchi");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentText("El bicho tiene una necesidad");
        notification.setSmallIcon(R.drawable.bicho_felis);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notification.setContentIntent(pendingIntent);
        notification.build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (nm != null) {
                Log.d(TAG, "notifica");
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
                nm.createNotificationChannel(mChannel);
                nm.notify(idUnica, notification.build());
            }
        }

    }
}



