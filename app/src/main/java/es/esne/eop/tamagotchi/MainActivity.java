package es.esne.eop.tamagotchi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity
{
    private static final String TAG = MainActivity.class.getCanonicalName();

    // Interfaz de la aplicación

    Button botonEat;
    Button botonSleep;
    Button botonPee;
    Button botonShower;
    ImageButton bicho;
    TextView estadoBicho;
    Button botonJugar;

    // Variable para saber el estado actual del tamagotchi
    // Esta empieza de primeras en Happy

    String state = "Happy";

    // Variable para comprobar si el tamagotchi está bien o necesita algo

    boolean isHappy = true;

    // Variable para comprobar el número de clicks que da el usuario a la pantalla

    int contadorClicks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicialización de todas las variables declaradas anteriormente:

        botonEat = findViewById(R.id.botonEat);
        botonPee = findViewById(R.id.botonPee);
        botonShower = findViewById(R.id.botonShower);
        botonSleep = findViewById(R.id.botonSleep);
        bicho = findViewById(R.id.bichoImg);
        estadoBicho = findViewById(R.id.estadoBicho);
        botonJugar = findViewById(R.id.botonJugar);

        Intent intent = getIntent();

        // La primera vez que abrimos la aplicación el tamagotchi será feliz
        // Sin embargo, si ya ha sido abierta recibirá el parámetro del intent CountdownService

        isHappy = intent.getBooleanExtra("TamagotchiHappiness", true);

        if(!isHappy)
        {
            // Obtenemos un estado random:

            state = TamagotchiStates.getTamagotchiState();

            Log.d(TAG, "Cambiamos imagen del bicho en función al estado random recibido.");
            switch (state) {
                case "Sleep":
                    bicho.setBackgroundResource(R.drawable.bicho_dormido);
                    estadoBicho.setText(R.string.Tired);
                    break;
                case "Eat":
                    bicho.setBackgroundResource(R.drawable.bicho_haambre);
                    estadoBicho.setText(R.string.Hungry);
                    break;
                case "Shower":
                    bicho.setBackgroundResource(R.drawable.bicho_sucio);
                    estadoBicho.setText(R.string.Dirty);
                    break;
                case "Pee":
                    bicho.setBackgroundResource(R.drawable.bicho_se_mea);
                    estadoBicho.setText(R.string.PeeNeeded);
                    break;
                case "Bored":
                    bicho.setBackgroundResource(R.drawable.bicho_iddle);
                    estadoBicho.setText(R.string.Bored);
                    break;
            }

            checkBotons();
        }

        comprobarDobleClick();
    }

    /**
     * Si el jugador hace un click en el tamagotchi y pasado 400 milisegundos no lo ha vuelto a
     * pulsar, se detecta ese click como un único click.
     * Si por el contrario lo hace antes, será detectado como doble click.
     */
    private void comprobarDobleClick()
    {
        bicho.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                contadorClicks++;
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        contadorClicks = 0;
                    }
                };
                if(contadorClicks == 1)
                    handler.postDelayed(runnable, 400);
                else
                if(contadorClicks == 2)
                {
                    Log.d(TAG, "Doble click realizado.");
                    Log.d(TAG, "El estado ahora es visible.");
                    estadoBicho.setVisibility(View.VISIBLE);
                    final Handler h = new Handler();
                    h.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Log.d(TAG, "El estado se hace invisible");
                            estadoBicho.setVisibility(View.INVISIBLE);
                        }
                    }, 3000);
                }else
                if(contadorClicks == 3)
                    contadorClicks = 0;
            }
        });
    }

    /**
     * Función que se llama una vez que el usuario sale de la aplicación.
     */
    @Override
    protected void onPause()
    {
        super.onPause();

        // Comprobamos si está feliz para cambiar su estado.

        if(isHappy)
        {
            // Creamos un intent para usar el servicio CountdownService.

            Intent intent = new Intent(getBaseContext(),CountdownService.class);
            Log.d(TAG, "Lanzamos servicio");
            startService(intent);
            isHappy = false;
        }
    }

    /**
     * Función para comprobar cuál de los botones hemos pulsado y si éste sirve para algo.
     * Esto se detecta mediante un switch con la variable state.
     */
    public void checkBotons()
    {
        // En todos los casos comprobamos dentro de los onClick si el tamagotchi está feliz para que
        // no funcione el botón cuando ya esté feliz el tamagotchi.

        switch (state)
        {
            case "Sleep":
                botonSleep.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                       if(!isHappy)
                       {
                           bicho.setBackgroundResource(R.drawable.bicho_durmiendo);
                           estadoBicho.setVisibility(View.VISIBLE);
                           estadoBicho.setText("ZzZZzzZzZZz");
                           final Handler handler = new Handler();
                           handler.postDelayed(new Runnable()
                           {
                               @Override
                               public void run()
                               {
                                   estadoBicho.setVisibility(View.INVISIBLE);
                                   SetBichoFelis();
                               }
                           }, 2000);
                       }
                    }
                });
                break;
            case "Eat":
                botonEat.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(!isHappy)
                        {
                            bicho.setBackgroundResource(R.drawable.bicho_comiendo);
                            estadoBicho.setVisibility(View.VISIBLE);
                            estadoBicho.setText("Cham cham");
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    estadoBicho.setVisibility(View.INVISIBLE);
                                    SetBichoFelis();
                                }
                            }, 2000);
                        }
                    }
                });
                break;
            case "Shower":
                botonShower.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(!isHappy)
                        {
                            bicho.setBackgroundResource(R.drawable.bicho_ducha);
                            estadoBicho.setVisibility(View.VISIBLE);
                            estadoBicho.setText("La la la la la la");
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    estadoBicho.setVisibility(View.INVISIBLE);
                                    SetBichoFelis();
                                }
                            }, 2000);
                        }
                    }
                });
                break;
            case "Pee":
                botonPee.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(!isHappy)
                        {
                            bicho.setBackgroundResource(R.drawable.bicho_pipi);
                            estadoBicho.setVisibility(View.VISIBLE);
                            estadoBicho.setText("Pssssssss");
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    estadoBicho.setVisibility(View.INVISIBLE);
                                    SetBichoFelis();
                                }
                            }, 2000);
                        }
                    }
                });
                break;

            // En este caso se lanza un intent nuevo ya que vamos a un activity que contiene el
            // minijuego de Piedra, Papel o Tijera.

            case "Bored":
                botonJugar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!isHappy)
                        {
                            Intent i = new Intent(getBaseContext(), MinijuegoActivity.class);
                            startActivity(i);
                        }
                    }
                });
        }
    }

    /**
     * Esta función resetea ciertas variables del tamagotchi para indicar que vuelve a estar feliz.
     */
    private void SetBichoFelis()
    {
        Log.d(TAG, "Reset del bicho");
        bicho.setBackgroundResource(R.drawable.bicho_felis);
        estadoBicho.setText(R.string.BeingHappy);
        state = "Happy";
        isHappy = true;
    }
}