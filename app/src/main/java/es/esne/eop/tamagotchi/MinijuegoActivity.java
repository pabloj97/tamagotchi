package es.esne.eop.tamagotchi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MinijuegoActivity extends Activity
{

    private static final String TAG = MainActivity.class.getCanonicalName();

    ImageButton botonPiedra;
    ImageButton botonPapel;
    ImageButton botonTijera;
    ImageView iaImagen;
    TextView texto;
    Button botonVolver;
    boolean flag = false;

    private static String[] jugadas = {
            "Piedra",
            "Papel",
            "Tijeras"
    };

    String state = "WAITING";
    String playerChoice;
    String iaChoice;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minijuego);

        botonPiedra = findViewById(R.id.piedraBoton);
        botonPapel = findViewById(R.id.papelBoton);
        botonTijera = findViewById(R.id.tijeraBoton);
        iaImagen = findViewById(R.id.IASeleccion);
        texto = findViewById(R.id.textoMinijuego);
        botonVolver = findViewById(R.id.botonVolver);

        checkBotons();

        botonVolver.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(flag)
                {
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private void setIaImage()
    {
        switch (iaChoice)
        {
            case "Piedra":
                iaImagen.setImageResource(R.drawable.piedra);
                break;
            case "Papel":
                iaImagen.setImageResource(R.drawable.papel);
                break;
            case "Tijeras":
                iaImagen.setImageResource(R.drawable.tijeras);
                break;
        }
    }

    /**
     * Comprobamos con los parámetros recibidos que jugador ha ganado.
     * @param playerChoice
     * @param iaChoice
     */
    private void checkResults(String playerChoice, String iaChoice)
    {
        if(playerChoice.equals(iaChoice))
            texto.setText(R.string.drawResult);
        else
        if( (playerChoice.equals("Piedra")  && iaChoice.equals("Tijeras")) ||
            (playerChoice.equals("Papel")   && iaChoice.equals("Piedra"))  ||
            (playerChoice.equals("Tijeras") && iaChoice.equals("Papel"))    )
            texto.setText(R.string.winResult);
        else
            texto.setText(R.string.loseResult);
        state = "END GAME";
        flag = true;
    }

    private void checkBotons()
    {
        botonTijera.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(state.equals("WAITING"))
                {
                    botonTijera.setScaleX(1.2f);
                    botonTijera.setScaleY(1.2f);
                    playerChoice = "Tijeras";
                    setIaGame();
                }
            }
        });
        botonPiedra.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(state.equals("WAITING"))
                {
                    botonPiedra.setScaleX(1.2f);
                    botonPiedra.setScaleY(1.2f);
                    playerChoice = "Piedra";
                    setIaGame();
                }
            }
        });
        botonPapel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(state.equals("WAITING"))
                {
                    botonPapel.setScaleX(1.2f);
                    botonPapel.setScaleY(1.2f);
                    playerChoice = "Papel";
                    setIaGame();
                }
            }
        });
    }

    private void setIaGame()
    {
        Log.d(TAG, "El jugador ha elegido una opción.");
        texto.setText(R.string.choiceDone);
        Log.d(TAG, "Entrando en estado IA GAME");
        state = "IA GAME";
        iaChoice = getRandomPlay();
        iaAnimationChoice();
    }

    public String getRandomPlay()
    {
        int randomIndex;
        randomIndex = new Random().nextInt(jugadas.length);
        return jugadas[randomIndex];
    }

    public void iaAnimationChoice()
    {
        Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                iaImagen.setImageResource(R.drawable.numero3);
                Handler h2 = new Handler();
                h2.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        iaImagen.setImageResource(R.drawable.numero2);
                        Handler h3 = new Handler();
                        h3.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                iaImagen.setImageResource(R.drawable.numero1);
                                Handler h4 = new Handler();
                                h4.postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        setIaImage();
                                        state = "CHECKING";
                                        Log.d(TAG, "Entrando en estado CHECKING");
                                        checkResults(playerChoice, iaChoice);
                                    }
                                },1000);
                            }
                        },1000);
                    }
                },1000);
            }
        }, 1000);
    }
}
