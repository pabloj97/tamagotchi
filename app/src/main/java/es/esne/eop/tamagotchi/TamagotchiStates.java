package es.esne.eop.tamagotchi;


import android.util.Log;

import java.util.Random;

public class TamagotchiStates {

    private static final String TAG = TamagotchiStates.class.getCanonicalName();

    /**
     * Posibles estados del tamagotchi
     */
    private static String[] STATE_TAMAGOTCHI={
            "Sleep",
            "Eat",
            "Shower",
            "Pee",
            "Bored",
            "Happy"
    };

    /**
     * Generación aleatoria de los estados
     * @return Respuesta generada de forma aleatoria
     */
    public static String getTamagotchiState(){
        int randomIndex;
        randomIndex = new Random().nextInt(STATE_TAMAGOTCHI.length);

        Log.d(TAG, "Devolvemos el estado random");
        return STATE_TAMAGOTCHI[randomIndex];
    }


}
